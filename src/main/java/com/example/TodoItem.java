package com.example;

public class TodoItem {
    private String todo;
    private int priority = 2;

    public TodoItem(){

    }
    public TodoItem(String todo){
        this.todo = todo;
    }
    public TodoItem(String todo, int priority){
        this.todo = todo;
        this.priority = priority;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTodo() {
        return todo;
    }

    public int getPriority() {
        return priority;
    }
}
