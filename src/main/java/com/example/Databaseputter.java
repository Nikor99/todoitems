package com.example;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Controller("/items")
public class Databaseputter {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/test";
    private static final String USERNAME = "testuser";
    private static final String PASSWORD = "test";
    @Post
    public void insertItem(String name, int priority) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // Open a connection to the database
            conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            // Prepare the SQL insert statement
            String sql = "INSERT INTO todo (name, priority) VALUES (?,?)";
            stmt = conn.prepareStatement(sql);

            // Set the values for the prepared statement
            stmt.setString(1, name);
            stmt.setInt(2, priority);

            // Execute the insert statement
            stmt.executeUpdate();
        } catch (SQLException e) {
            // Handle any errors that may have occurred
            e.printStackTrace();
        } finally {
            // Close the resources
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
        }
    }
    @Get("/todos")
    public List<TodoItem> getTodos() throws SQLException {
        // Establish a connection to the database
        Connection conn = null;
        conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

        // Create a PreparedStatement to execute the SQL query
        PreparedStatement stmt = conn.prepareStatement("SELECT name, priority FROM todos");

        // Execute the query and retrieve the results
        ResultSet rs = stmt.executeQuery();

        // Iterate over the results and add each one to the list of todos
        List<TodoItem> todos = new ArrayList<>();
        while (rs.next()) {
            String name = rs.getString("name");
            int priority = rs.getInt("priority");
            todos.add(new TodoItem(name, priority));
        }

        return todos;
    }

    @Delete("/todo/{name}")
    public void deleteTodo(String name) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // Open a connection to the database
            conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            // Prepare the SQL insert statement
            String sql = "DELETE FROM todo WHERE name = ?";
            stmt = conn.prepareStatement(sql);

            // Set the values for the prepared statement
            stmt.setString(1, name);

            // Execute the insert statement
            stmt.executeUpdate();
        } catch (SQLException e) {
            // Handle any errors that may have occurred
            e.printStackTrace();
        } finally {
            // Close the resources
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
        }
    }
}
